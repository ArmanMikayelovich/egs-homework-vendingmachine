package com.energizeglobal.internship;


import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class ExchangerTest {


    @Test
    public void getChangeWithZeroArguments() {
        assertArrayEquals(new int[]{0, 0, 0, 0, 0, 0}, Exchanger.getChange(0, 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getChangeWithIllegalCashArgument() {
        assertArrayEquals(new int[]{0, 0, 0, 0, 0, 0}, Exchanger.getChange(-1, 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getChangeWithIllegalPriceArgument() {
        assertArrayEquals(new int[]{0, 0, 0, 0, 0, 0}, Exchanger.getChange(-1, 1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getChangeWithAllIllegalArguments() {
        assertArrayEquals(new int[]{0, 0, 0, 0, 0, 0}, Exchanger.getChange(-1, -1));
    }

    @Test
    public void getChangeWithNormalArguments() {
        assertArrayEquals(new int[]{0, 1, 1, 1, 1, 14}, Exchanger.getChange(15, 0.1));
    }

    @Test
    public void getChangeWithNormalArguments2() {
        assertArrayEquals(new int[]{1, 1, 1, 1, 1, 14}, Exchanger.getChange(15, 0.09));
    }

    @Test
    public void getChangeWithIntegerMaxArgument() {
        assertArrayEquals(new int[]{4, 0, 2, 1, 1, Integer.MAX_VALUE - 1},
                Exchanger.getChange(Integer.MAX_VALUE, 0.01));
    }
    @Test
    public void getChangeWithDoubleMaxArgument() {
        assertArrayEquals(new int[]{0, 0, 1, 0, 0, 2147482624},
                Exchanger.getChange(Integer.MAX_VALUE, Double.MAX_EXPONENT - 0.1));
    }
}
