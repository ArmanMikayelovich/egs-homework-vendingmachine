package com.energizeglobal.internship;

public class Exchanger {

    private static final int[] COINS = {1, 5, 10, 25, 50, 100};

    public static int[] getChange(final double cash, final double price) {
        checkValidation(cash, price);
        long changeInCoins = getChangeInCoins(cash, price);
        int[] countOfAllCoins = new int[6];
        for (int offset = COINS.length - 1; offset >= 0; offset--) {
            int count = (int) (changeInCoins / COINS[offset]);
            if (count > 0) {
                countOfAllCoins[offset] = count;
                changeInCoins = changeInCoins - (count * COINS[offset]);
            }
        }
        return countOfAllCoins;
    }

    private static void checkValidation(double cash, double price) {
        if (price > cash ||
                cash < 0 || price < 0) {
            throw new IllegalArgumentException("Correct your arguments!.");
        }
    }

    private static long getChangeInCoins(double cash, double price) {
        long cashInCoins = (long) cash * 100;
        int priceInCoins = (int) (price * 100);
        return cashInCoins - priceInCoins;
    }


}
